package com.inm.msurveyapi.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inm.msurveyapi.model.Request;
import com.inm.msurveyapi.model.TblUssdCalls;
import com.inm.msurveyapi.model.oba;
import com.inm.msurveyapi.services.TblUssdCallsService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	@Autowired
	TblUssdCallsService tblUssdCallsService;
	static volatile String message ="";

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String name = authentication.getName();
		String password = authentication.getCredentials().toString();
		ArrayList<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
		User appUser = null;
		if (name.trim().equals("user") && password.trim().equals("password")) {
			try {
				test("254722715026");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));
			// use the credentials
			appUser = new User(name, password, true, true, true, true, grantedAuths);
		} else if (name.trim().equals("admin") && password.trim().equals("password")) {
			try {
				test("254722715026");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//getDbUpdate();
			grantedAuths.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			// use the credentials
			appUser = new User(name, password, true, true, true, true, grantedAuths);
			
		} else {
			return null;
		}
		return new UsernamePasswordAuthenticationToken(appUser, password, grantedAuths);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	void test(String phone) throws InterruptedException {
		String url = "https://mauth.myriadgroup.mobi/api/v1.0/auth/" + phone;
		ObjectMapper objectMapper = new ObjectMapper();
		String[] opt = { "Yes", "No" };
		oba oba = new oba("Do you want to log in?", opt, 5000, "https://posthere.io/fc15-41a0-a5ef");

		
		Request req = new Request(oba);
		String reqAsString = "";
		try {
			reqAsString = objectMapper.writeValueAsString(req);

			CloseableHttpClient client = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);

			StringEntity entity = new StringEntity(reqAsString, ContentType.APPLICATION_JSON);

			httpPost.setHeader("auth", "2a68467e-61b9-4f92-9733-910371808ab1");
			httpPost.setHeader("sid", "478ffc4e-82e8-311a-8297-a6a3468484c5");
			httpPost.setEntity(entity);

			CloseableHttpResponse response = client.execute(httpPost);
			
			//getDbUpdate();
			if (response.getStatusLine().getStatusCode() == 200) {

				// String token = response.get
				// System.out.println("success \n" + response);
				String token = EntityUtils.toString(response.getEntity());
				System.out.println(token);

			} else if (response.getStatusLine().getStatusCode() == 201) {
				String token = EntityUtils.toString(response.getEntity());
				System.out.println(token);
				
				int b = token.lastIndexOf('\"'); 
				System.out.println(token.substring(7,b));
				
				getDbUpdate(token);
			}else {
				System.out.println("fail \n" + response);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(reqAsString);
	}
	
	String getDbUpdate(String id) throws InterruptedException{
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		ScheduledExecutorService waitingexecutor = Executors.newSingleThreadScheduledExecutor();
		
		String success ="";
		TimerTask waitingTime = new TimerTask() {
			 public void run() {
				 for(int x = 0; x<10;x++) {
						TimerTask repeatedTask = new TimerTask() {
					        public void run() {
					        	TblUssdCalls tblUssdCalls = tblUssdCallsService.findById(id);
					        	if(tblUssdCalls != null && tblUssdCalls.getStatus()!= null && tblUssdCalls.getStatus() == 3) {
					        		if (tblUssdCalls.getMessage().trim().equals("1")) {
					        			message = "Suceess";
					        			System.out.println(message);
					        		}
					        	}else {
					        		message = "Failure";
					        		System.out.println(message);
					        	}
					            System.out.println("Task performed on " + new Date());
					        }
					    };		    
					    long delay  = 1*1000*1000 * 1L;
					    long period = 1000L;
					    executor.scheduleAtFixedRate(repeatedTask, delay, period, TimeUnit.MILLISECONDS);
					    try {
							Thread.sleep(delay + period * 3);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				    executor.shutdown();
		        }
		};
		long delaywaiting  = 1000L;
	    long periodwaiting = 1000L;
	    executor.scheduleAtFixedRate(waitingTime, delaywaiting, periodwaiting, TimeUnit.MILLISECONDS);
	    Thread.sleep(delaywaiting + periodwaiting * 3);
		waitingexecutor.shutdown();
		
		return message;
		
	}

}
