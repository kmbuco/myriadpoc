package com.inm.msurveyapi.daoimpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.inm.msurveyapi.dao.TblUssdCallsDao;
import com.inm.msurveyapi.model.TblUssdCalls;


public class TblUssdCallsDaoImpl implements TblUssdCallsDao{

	
	TblUssdCallsDao tblUssdCallsDao;
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Override
	public <S extends TblUssdCalls> S save(S entity) {
		return tblUssdCallsDao.save(entity);
	}

	@Override
	public <S extends TblUssdCalls> Iterable<S> save(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TblUssdCalls findOne(Integer id) {
		// TODO Auto-generated method stub
		return tblUssdCallsDao.findOne(id);
	}

	@Override
	public boolean exists(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<TblUssdCalls> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<TblUssdCalls> findAll(Iterable<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TblUssdCalls entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Iterable<? extends TblUssdCalls> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TblUssdCalls findById(String id) {
		// TODO Auto-generated method stub
		return tblUssdCallsDao.findById(id);
	}

}
