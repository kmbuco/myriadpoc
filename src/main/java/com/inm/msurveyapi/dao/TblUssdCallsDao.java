package com.inm.msurveyapi.dao;

import org.springframework.data.repository.CrudRepository;

import com.inm.msurveyapi.model.TblUssdCalls;

public interface TblUssdCallsDao extends CrudRepository<TblUssdCalls, Integer>{
	public TblUssdCalls findById(String id);
}
