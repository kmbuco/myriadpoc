package com.inm.msurveyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyriadDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyriadDemoApplication.class, args);
	}
}
