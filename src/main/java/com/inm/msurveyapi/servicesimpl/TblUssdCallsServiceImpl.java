package com.inm.msurveyapi.servicesimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.inm.msurveyapi.dao.TblUssdCallsDao;
import com.inm.msurveyapi.model.TblUssdCalls;
import com.inm.msurveyapi.services.TblUssdCallsService;

@Component
public class TblUssdCallsServiceImpl implements TblUssdCallsService{

	@Autowired
	TblUssdCallsDao tblUssdCallsDao;
	
	@Override
	public TblUssdCalls Save(TblUssdCalls tblUssdCalls) {
		
		return tblUssdCallsDao.save(tblUssdCalls);
	}

	@Override
	public Integer checkStatus(String id) {
		
		TblUssdCalls tblUssdCalls = tblUssdCallsDao.findById(id);
		
		Integer status = tblUssdCalls.getStatus();
		
		//4 - outgoing stop session
		return status;
	}

	@Override
	public TblUssdCalls findById(String id) {
		// TODO Auto-generated method stub
		return tblUssdCallsDao.findById(id);
	}
	

}
