package com.inm.msurveyapi.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TblUssdCalls {
	
	public Integer getUssdID() {
		return UssdID;
	}
	public void setUssdID(Integer ussdID) {
		UssdID = ussdID;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		return "Description: "+description+", Id: "+id+", message: "+message+", status: "+status+", UssdID: "+UssdID;	
	}
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	Integer UssdID;
	Integer status;
	String message;
	String id;
	String description;
}
