package com.inm.msurveyapi.model;

public class oba {
	String prompt;
	String[] answers;
	Integer timeout;
	String callback;
	
	public oba(String prompt,String[] answers,Integer timeout,String callback) {
		this.prompt = prompt;
		this.answers = answers;
		this.timeout = timeout;
		this.callback = callback;
	}
	public String getPrompt() {
		return prompt;
	}
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}
	public String[] getAnswers() {
		return answers;
	}
	public void setAnswers(String[] answers) {
		this.answers = answers;
	}
	public Integer getTimeout() {
		return timeout;
	}
	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}
	public String getCallback() {
		return callback;
	}
	public void setCallback(String callback) {
		this.callback = callback;
	}
	
}
