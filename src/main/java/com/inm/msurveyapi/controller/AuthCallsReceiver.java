package com.inm.msurveyapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inm.msurveyapi.model.TblUssdCalls;
import com.inm.msurveyapi.model.UssdCalls;
import com.inm.msurveyapi.services.TblUssdCallsService;


@RestController
public class AuthCallsReceiver {
	
	@Autowired
	TblUssdCallsService tblUssdCallsService;
	
	@RequestMapping(value = "/receiver",method = RequestMethod.POST)
	int Receiver(@RequestBody UssdCalls calls) {
		
		TblUssdCalls tblUssdCalls = new TblUssdCalls();
		tblUssdCalls.setDescription(calls.getDescription());
		tblUssdCalls.setId(calls.getId());
		tblUssdCalls.setMessage(calls.getMessage());
		tblUssdCalls.setStatus(calls.getStatus());
		tblUssdCallsService.Save(tblUssdCalls);
		tblUssdCalls.toString();
		
		return 0;
		
	}
}
