package com.inm.msurveyapi.services;

import com.inm.msurveyapi.model.TblUssdCalls;

public interface TblUssdCallsService {
	public TblUssdCalls Save(TblUssdCalls tblUssdCalls);
	public Integer checkStatus(String id);
	public TblUssdCalls findById(String id);
}
